package com.lastminute

import org.junit.Assert.assertThat
import org.junit.Test
import org.hamcrest.core.Is.`is` as sameAs

class ComponentTest {

    @Test
    fun `test a simple interaction`() {
        assertThat(Component().aFunction(), sameAs(false))
    }
}